# zfs_backup

Utility for creating backups with zfs.

## Geting started

1. Download zfs_backup.py and store in at /usr/local/bin/zfs_backup
1. Create a configuration file (e.g. in /etc/zfs_backup.yaml)
3. Make a backup with

```
/usr/local/bin/zfs_backup.py  -c /etc/zfs_backup.yaml -b daily
```
The zfs_backup creates for each dataset a snapshot in the zfs pool.
The snapshot has suche a name:

```
<zfs-pool>/<dataset>@<prefix>-<backup_type>-<timestamp-of-backup>
 |          |         |        |             +--- Create timestamp of the backup
 |          |         |        +----------------- Type of the backup (daily, weekly, monthly or yearly)
 |          |         +-------------------------- Prefix of the snap (defined in configuraiton file with `prefix`)
 |          +------------------------------------ Name of the ZFS dataset to backup
 +----------------------------------------------- Name of the zpool
```

Example:

```
rpool/data@backup-daily-2024-04-01T10:03:00
```

Afterwards it sends the snap to the zfs backup pool.
As final step it destroy the obsolete zfs snapshost from zfs pool and the backup pool.

## Restore

For restoring a backup destroy the original zfs dataset and use zfs send and receive to restore it.
Example:

```bash
$ zfs destroy rpool/data
$ zfs create rpool/data
$ zfs send backup/data/backup-daily-2024-04-02T10:03:02 | zfs receive rpool/data
```

## Configuration file

The zfs_backup util requires a yaml configuration file with the following content:

```yaml
# Name of the zfs pool to backup
dataPool: rpool

# Name of the zfs pool where the backup is stored
backupPool: backup

# Prefix of the backup. 
prefix: backup

# List of the datasets of the zfs pool that are backed up
datasets:
  - test

#
# Number of snapshot to keep for each backup type
# 
daily: 7
weekly: 4
monthly: 12
yearly: 10

# File where the locks are stored
logFile: /var/log/zfs_backup.log

# lock file to avoid duplicate processing
runFile: /var/lock/zfs_backup.lck

```
